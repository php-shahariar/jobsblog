@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if($message = Session::get('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            <div class="card">
                <div class="card-header">Job Application list</div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>SL</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Resume</th>
                        </tr>
                        @foreach($appliedJobs as $key => $appliedJob)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $appliedJob->applicant->first_name }} {{ $appliedJob->applicant->last_name }}</td>
                            <td>{{ $appliedJob->applicant->email }}</td>
                            <td>
                                @if(!empty($appliedJob->applicant->resume))
                                <button class="btn btn-info">{{ $appliedJob->applicant->resume }}</button>
                                @else
                                    No Resume Found
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
