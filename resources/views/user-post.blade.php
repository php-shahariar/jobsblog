@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if($message = Session::get('message'))
                    <div class="alert alert-success">{{ $message }}</div>
                @endif
                <div class="card">
                    <div class="card-header">
                        Users Post
                        <a href="{{ route('admin.dashboard') }}" class="btn btn-md btn-primary float-right">User List</a>
                    </div>

                    <div class="card-body">
                        @foreach($posts as $post)
                            <h2>{{ $post->title }}</h2>
                            <p>{!! $post->description !!}</p>
                            <span>Created By : {{ $post->user->name }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
