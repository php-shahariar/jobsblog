@extends('layouts.applicant')

@section('content')
    <div class="container">
        <div class="col-md-12">
            @if(Session::get('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
                @if(Session::get('error'))
                    <p class="alert alert-danger">{{ Session::get('error') }}</p>
                @endif
            <div class="card">
                <div class="card-header">
                    @if(Session::get('id'))
                    {{ Session::get('first_name') }} {{ Session::get('last_name') }} Dashboard
                    @endif
                    <a href="{{ route('all.jobs.circular') }}" class="float-right">All Job circular</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('applicant.profile.update', $applicant->id) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ $applicant->first_name }}" required autocomplete="first_name" autofocus>

                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ $applicant->last_name }}" required autocomplete="last_name" autofocus>

                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="skills" class="col-md-4 col-form-label text-md-right">Skills</label>

                            <div class="col-md-6">
                                <input id="skills" type="text" class="form-control @error('skills') is-invalid @enderror" name="skills" value="{{ $applicant->skills }}" required autocomplete="skills" autofocus>

                                @error('skills')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $applicant->email }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Profile Picture</label>

                            <div class="col-md-6">
                                <input id="profile_picture" type="file" class="form-control" name="profile_picture" required>
                                <img src="{{ asset('/profile-pictures/'.$applicant->profile_picture) }}" height="100" width="100">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Resume</label>

                            <div class="col-md-6">
                                <input id="resume" type="file" class="form-control" name="resume" required accept="application/pdf">
                                <span class="btn btn-info">{{ $applicant->resume }}</span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
