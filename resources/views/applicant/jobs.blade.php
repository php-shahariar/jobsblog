@extends('layouts.applicant')

@section('content')
    <div class="container">
        @if(Session::get('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::get('error'))
            <p class="alert alert-danger">{{ Session::get('error') }}</p>
        @endif
        <div class="row">
            @foreach($jobs as $key => $job)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{ $job->job_title }}</div>
                    <div class="card-body">
                        <p>Details : {{ $job->job_description }}</p>
                        <p>Office Location : {{ $job->location }}</p>
                        <p>Country : {{ $job->country }}</p>
                        <p>Salary : {{ number_format($job->salary, 2) }} BDT</p>
                    </div>
                    <form action="{{ route('job.applied', $job->id) }}" method="post">
                        @csrf
                        <input type="hidden" name="post_id" value="{{ $job->id }}">
                        <input type="hidden" name="user_id" value="{{ $job->user->id }}">
                        <button type="submit" class="btn btn-block btn-primary">Apply</button>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection
