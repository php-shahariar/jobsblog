@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if($message = Session::get('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            <div class="card">
                <div class="card-header">
                    User Dashboard
                    <a href="{{ route('all.applied.jobs') }}" class="float-right">All Applied jobs</a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('post.create') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" placeholder="Post Title">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea type="text" name="description" class="form-control" rows="5" placeholder="Post description"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Salary</label>
                            <input type="text" name="salary" class="form-control" placeholder="Salary">
                        </div>
                        <div class="form-group">
                            <label>Location</label>
                            <textarea type="text" name="location" class="form-control" rows="5" placeholder="Job location"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="country">
                                <option value="bangladesh">Bangladesh</option>
                                <option value="pakistan">Pakistan</option>
                                <option value="usa">USA</option>
                                <option value="canada">Canada</option>
                            </select>
                        </div>
                        <button type="submit" name="btn" class="btn btn-success btn-lg">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
