<?php

namespace App\Http\Controllers;

use App\JobPost;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::with('post')->has('post')->paginate(3);
        return view('admin', compact('users'));
    }

    public function showUsersPost()
    {
        $posts = JobPost::with('user')->where('status', 1)->get();
        return view('user-post', compact('posts'));
    }
}
