<?php

namespace App\Http\Controllers;

use App\JobPost;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'location' => 'required',
                'country' => 'required',
                'salary' => 'required',
            ]);

            $post = new JobPost();
            $post->user_id = Auth::user()->id;
            $post->job_title = $request->title;
            $post->job_description = $request->description;
            $post->salary = $request->salary;
            $post->location = $request->location;
            $post->country = $request->country;
            $post->save();
            return redirect()->back()->with('message', 'Post create successfully');
        }catch (\Exception $exception) {
            return  redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
