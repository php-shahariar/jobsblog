<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Applied;
use App\JobPost;
use Illuminate\Http\Request;
use Session;
class ApplicantController extends Controller
{
    public function showApplicantLoginForm()
    {
        return view('applicant.login');
    }

    public function applicantLoginSystem(Request $request)
    {

        $applicant = Applicant::where('email', $request->email)->first();
        if ($applicant){
            if (password_verify($request->password, $applicant->password)){
                Session::put('id', $applicant->id);
                Session::put('first_name', $applicant->first_name);
                Session::put('last_name', $applicant->last_name);
                return redirect('applicant-dashboard');
            }else{
                return redirect()->back()->with('message', 'Password dose not match');
            }

        }else{
            return redirect()->back()->with('message', 'E-mail dose not match');
        }

    }

    public function showApplicantRegForm()
    {
        return view('applicant.register');
    }

    public function showApplicantReg(Request $request)
    {
        try {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:applicants',
                'password' => 'required',
            ]);

            $applicant = new Applicant();
            $applicant->first_name = $request->first_name;
            $applicant->last_name = $request->last_name;
            $applicant->email = $request->email;
            $applicant->password = bcrypt($request->password);
            $applicant->save();
            Session::put('id', $applicant->id);
            Session::put('first_name', $applicant->first_name);
            Session::put('last_name', $applicant->last_name);
            return redirect('applicant-dashboard');

        }catch (\Exception $exception) {
            return  redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function showApplicantDashboard()
    {
        $applicant = Applicant::where('id', Session::get('id'))->first();
        return view('applicant.dashboard', compact('applicant'));
    }

    public function applicationProfileUpdate(Request $request, Applicant $applicant)
    {
        try {
//            $this->validate($request, [
//                'first_name' => 'required',
//                'last_name' => 'required',
//                'password' => 'required',
//            ]);

            $profilePic = time().'_'.'.'. $request->profile_picture->extension();
            $request->profile_picture->move(public_path('profile-pictures'), $profilePic);

            $resume = time().'_'.'.'. $request->resume->extension();
            $request->resume->move(public_path('resume'), $resume);

            $applicant->first_name = $request->first_name;
            $applicant->last_name = $request->last_name;
            $applicant->email = $request->email;
            $applicant->skills = $request->skills;
            $applicant->password = bcrypt($request->password);
            $applicant->profile_picture = $profilePic;
            $applicant->resume = $resume;
            $applicant->save();
            Session::put('id', $applicant->id);
            Session::put('first_name', $applicant->first_name);
            Session::put('last_name', $applicant->last_name);
            return redirect()->back()->with('message', 'profile updated');

        }catch (\Exception $exception) {
            return  redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function showAllJobsCircular()
    {
        $jobs = JobPost::with('user')->get();
        return view('applicant.jobs', compact('jobs'));
    }

    public function jobApplied(Request $request)
    {
        $applied = new Applied();
        $applied->user_id = $request->user_id;
        $applied->post_id = $request->post_id;
        $applied->applicant_id = Session::get('id');
        $applied->status = 1;
        $applied->save();
        return redirect()->back()->with('message', 'job applied successfully');
    }

    public function applicantLogout(){
        Session::forget('id');
        Session::forget('name');
        return redirect('/');
    }
}
