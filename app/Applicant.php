<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    public function applied()
    {
        return $this->hasMany(Applied::class, 'user_id', 'id');
    }
}
