<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function applied()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function getCountryAttribute($value)
    {
        return ucfirst($value);
    }
}
