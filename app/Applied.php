<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applied extends Model
{
    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function post()
    {
        return $this->hasMany(JobPost::class);
    }

    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
