<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/all-applied-jobs', 'HomeController@allJobApplied')->name('all.applied.jobs');

Route::post('/post-create', 'PostController@store')->name('post.create');
Route::get('/admin-dashboard', 'AdminController@index')->name('admin.dashboard');
Route::get('/users-post', 'AdminController@showUsersPost')->name('users.post');
Route::get('/applicant-login', 'ApplicantController@showApplicantLoginForm')->name('applicant.login');
Route::get('/applicant-register', 'ApplicantController@showApplicantRegForm')->name('applicant.register');
Route::post('/applicant-registration', 'ApplicantController@showApplicantReg')->name('applicant.registration');
Route::post('/applicant/login', 'ApplicantController@applicantLoginSystem')->name('applicant.login.system');
Route::post('/applicant-profile-update/{applicant}', 'ApplicantController@applicationProfileUpdate')->name('applicant.profile.update');
Route::get('/applicant-dashboard', 'ApplicantController@showApplicantDashboard')->name('applicant.dashboard');
Route::get('/all-jobs-circular', 'ApplicantController@showAllJobsCircular')->name('all.jobs.circular');
Route::post('/job-applied', 'ApplicantController@jobApplied')->name('job.applied');
Route::post('/applicant.logout', 'ApplicantController@applicantLogout')->name('applicant.logout');
